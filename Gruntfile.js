module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            folder: ['build/']
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'src/css',
                    src: ['*.css ', '**/*.css'],
                    dest: 'build/css/'
                }]
            }
        },
        htmlmin: { // Task
            dist: { // Target
                options: { // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: { // Dictionary of files
                    'build/index.html': 'src/index.html', // 'destination': 'source'
                }
            }
        },
        openui5_preload: {
            component: {
                options: {
                    resources: {
                        cwd: 'src',
                        prefix: 'org/roar',
                        src: [
                            '**/*.js',
                            '**/**/*.js',
                            '**/**/*.json',
                            '!libs/**/*.js',
                            '**/*.fragment.html',
                            '**/*.fragment.json',
                            '**/*.fragment.xml',
                            '**/*.view.html',
                            '**/*.view.json',
                            '**/*.view.xml',
                            '**/*.properties',
                            '!resources/**/*.*',
                        ]
                    },
                    dest: 'build',
                    compress: true
                },
                components: true
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {
                        expand: true,
                        cwd: 'src/',
                        src: ['manifest.appcache'],
                        dest: 'build/'
                    }, {
                        expand: true,
                        cwd: 'src/',
                        src: ['libs/**/*'],
                        dest: 'build/'
                    }
                ]
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    // grunt.loadNpmTasks('grunt-contrib-uglify');

    //css minification
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    //ui5 tasks
    grunt.loadNpmTasks('grunt-openui5');

    //copy task
    grunt.loadNpmTasks('grunt-contrib-copy');

    // html minify
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    grunt.loadNpmTasks('grunt-contrib-clean');

    // Default task(s).
    grunt.registerTask('default', ['clean', 'cssmin', 'openui5_preload', 'copy', 'htmlmin']);
};
