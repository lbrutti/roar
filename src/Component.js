sap.ui.define(['sap/ui/core/UIComponent','org/roar/libs/lodash.js/4.13.1/lodash.min','org/roar/libs/q.js/1.4.1/q.min'],
	function(UIComponent,_,Q) {
	"use strict";

	var Component = UIComponent.extend("org.roar.Component", {

		metadata : {
			rootView : "org.roar.view.Page",
			dependencies : {
				libs : [
					"sap.m"
				]
			},
			config : {
				sample : {
					stretch : true,
					files : [
						"Page.view.xml",
						"Page.controller.js"
					]
				}
			}
		}
	});

	return Component;

});
