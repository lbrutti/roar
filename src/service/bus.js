sap.ui.define(["sap/ui/core/library"], function (core) {
	return (function(core){
		var bus = core.Core().getEventBus();
        var CHANNEL = "service.defaultChannel";
		return {
            publish: publish,
            subscribe: subscribe,
            unsubscribe: unsubscribe
        };
        function publish(sTopic, oData){
            bus.publish(CHANNEL,sTopic,oData);
        }
        function subscribe(sTopic, fSubscriber){
            bus.subscribe(CHANNEL,sTopic,fSubscriber);
        }
        function unsubscribe(sTopic, fSubscriber) {
            bus.unsubscribe(CHANNEL,sTopic,fSubscriber);
        }
	})(core);
});
