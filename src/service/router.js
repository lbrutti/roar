sap.ui.define([
    'org/roar/service/bus',
    "sap/ui/core/library",
    "sap/ui/core/routing/History"
], function(bus, core, History) {
    return (function(bus, core, History) {
        var router = undefined;
        bus.subscribe('roar.hasRouter', hasRouter);

        return {
            getRouter: getRouter
        };

        function getRouter() {
            return router;
        }

        function hasRouter(c, e, params) {
            router = params.router;
            getRouter().onNavBack = _.partialRight(onNavBack, History);
        }

        function onNavBack(oEvent, History) {
            var oHistory, sPreviousHash;

            oHistory = History.getInstance();
            sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            }
        }
    })(bus, core, History);
});
