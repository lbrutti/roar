sap.ui.define([], function() {
    "use strict";
    var voice = new Audio();

    return function() {

        return (function() {
            var sound;
            var name;
            var pic;
            voice.preload = true;

            function speak() {
                voice.pause();
                voice.src = getSound();
                voice.play();
            }

            function create(data) {
                sound = data.soundPath;
                name = data.name;
                pic = data.imgPath;
            }

            function getName() {
                return name;
            }

            function getPic() {
                return pic;
            }

            function getSound() {
                return sound;
            }

            function getData() {
                return {
                    name: getName(),
                    pic: getPic()
                };
            }
            return {
                speak: speak,
                create: create,
                getData: getData,
                getName: getName
            };
        })();
    };
});
