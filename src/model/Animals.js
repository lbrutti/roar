  sap.ui.define([
    'org/roar/service/libs/lodash',
    'org/roar/service/libs/q',
    'sap/ui/model/json/JSONModel',
    'org/roar/model/Animal'
], function(_, Q, JSONModel, Animal) {
    "use strict";

    return (function(_, Q, JSONModel, Animal) {
        var animals;
        var model = new JSONModel();

        function loadAnimals(sPath) {
            var defer = Q.defer();
            $.getJSON(sPath, function(data) {
                animals = _.map(data, function(o) {
                    var animal = new Animal();
                    animal.create(o);
                    return animal;
                });
                model.setData(_.map(animals, function(a) {
                    return a.getData();
                }));
                defer.resolve();
            }, function(err) {
                defer.reject(err);
            });
            return defer.promise;
        }

        function getAnimals() {
            return animals;
        }

        function getModel() {
            return model;
        }

        function getAnimalByName(sName) {
            return _.find(animals, function(animal) {
                return animal.getName() === sName;
            });
        }

        function getInstance() {
            return {
                loadAnimals: loadAnimals,
                getAnimals: getAnimals,
                getModel: getModel,
                getAnimalByName: getAnimalByName
            };
        }

        return getInstance();
    })(_, Q, JSONModel, Animal);

});
