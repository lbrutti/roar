sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/mvc/Controller',
    'sap/ui/model/json/JSONModel',
    "org/roar/service/bus",
    "org/roar/service/router",
      "org/roar/model/Animals"
], function(jQuery, Controller, JSONModel, bus, router,Animals) {
    "use strict";

    return Controller.extend("org.roar.controller.Page", (function() {
        var controller;
        var sPath;
        var oRouter;

        function onInit(evt) {
            /*jshint validthis: true */
            controller = this;
            sPath = jQuery.sap.getModulePath("org.roar", "/../resources/roar.json");
            Animals.loadAnimals(sPath).then(function(){
              controller.getView().setModel(Animals.getModel(), "Animals");
            });

        }

        function onTilePress(evt) {
            var animalData = evt.getSource().getBindingContext("Animals").getObject();
            var animal = Animals.getAnimalByName(animalData.name);
            animal.speak();
        }

        return {
            onInit: onInit,
            onTilePress: onTilePress
        };
    })());
});
